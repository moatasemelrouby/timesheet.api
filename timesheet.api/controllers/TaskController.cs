﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/v1/task")]
    [ApiController]
    public class taskController : ControllerBase
    {
        private ITaskService _taskService;
        public taskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

       
        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var items = this._taskService.GetTasks();
            return new ObjectResult(items);
        }
    }
}